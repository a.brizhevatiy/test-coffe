import React, {useEffect, useState} from 'react';
import Navigation from "../../components/Navigation";
import Top from "../../components/Top";
import Middle from "../../components/Middle";
import Description from "../../components/Description";
import VideoPlayer from "../../components/Video";
import Shop from "../Shop";
import Mission from "../../components/Mission";
import Rekl from "../../components/Rekl";
import GalleryNew from "../../components/GalleryNew";
import ContactBlock from "../../components/ContactBlock";
import Footer from "../../components/Footer";
import coffeBig from "../../Images/coffee-big.webp";
import CallBtn from "../../components/CallBtn";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowCircleUp} from "@fortawesome/free-solid-svg-icons";
import CartList from "../../components/CartList";
import "./index.scss"
import {useSelector} from "react-redux";

const Page = () => {
    const [coord, setCoord] = useState(0)

    const showCart = useSelector(state => state.cart.show)

    useEffect(() => {
        window.addEventListener('scroll', () => setCoord(window.pageYOffset))
        return window.removeEventListener('scroll', () => setCoord(window.pageYOffset))
    }, [])

    const videoJsOptions = {
        autoplay: true,
        playbackRates: [0.5, 1, 1.25, 1.5, 2],
        controls: true,
        poster: coffeBig,
        sources: [
            {
                src: '//vjs.zencdn.net/v/oceans.mp4',
                type: 'video/mp4',
            },
        ],
    };
    return (
        <div className="page">
            <div className="lines">
                <div className="lines-n"></div>
                <div className="lines-n"></div>
                <div className="lines-n"></div>
                <div className="lines-n"></div>
                <div className="lines-n"></div>
            </div>
            {showCart && <CartList/>}
            <Navigation/>
            <Top/>
            <Middle/>
            <Description/>
            <VideoPlayer {...videoJsOptions}/>
            <Shop/>
            <Mission/>
            <Rekl/>
            <GalleryNew/>
            <ContactBlock/>
            <Footer/>
            <CallBtn/>
            {coord > 300 && <div className="arrow__up">
                <a href="#main"><FontAwesomeIcon icon={faArrowCircleUp}/></a>
            </div>}
        </div>
    );
}

export default Page;
