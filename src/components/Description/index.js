import React from 'react';
import Post from "../Post"
import descImage from "../../Images/descriptionImage.png"
import "./index.scss"

const Description = () => {
    const postContent1 = {
        title: "Качество",
        text: "Направление нашей команды работает на стабильность качества продукта. Вкус кофе, который Вы пробовали, всегда будет одинаково премиальным!"
    }
    const postContent2 = {
        title: "Цена",
        text: "Благодаря профессиональному оборудованию, на нашем производстве были минимизированны все возможные затраты и Мы создаем Премиальный продукт по конкурентной цене!"
    }
    const postContent3 = {
        title: "Команда",
        text: "Все вышеперечисленное достигается благодаря правильной работе гашей команды. се сотрудники прошли европейсекое обучение SCA! "
    }
    const postContent4 = {
        title: "Доставка",
        text: "Благодаря правильной статистике и логистике, все заказы в кофейни поставляются в день заказа или на следующий!"
    }


    return (
        <div className="description">
            <div className="description__wrapper">
                <div className="description__title">
                    <h2>Кофе свежей обжарки</h2>
                    <p className="description__title--text">Почему клиенты выбирают именно Нас</p>
                    <div className="underline"></div>
                </div>
                <div className="description__body">
                    <div className="description__tab">
                        <Post title={postContent1.title} text={postContent1.text}/>
                        <Post title={postContent3.title} text={postContent3.text}/>
                    </div>
                    <div className="description__tab">
                        <div className="description__image"><img src={descImage} alt=""/></div>

                    </div>
                    <div className="description__tab">
                        <Post title={postContent2.title} text={postContent2.text}/>
                        <Post title={postContent4.title} text={postContent4.text}/>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Description;
