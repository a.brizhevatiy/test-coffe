import React from 'react';
import "./index.scss"

const PlayButton = ({func}) => {
    return (
        <div className="playBtn">
            <div className="playBtn__wrapper"  onClick={func}>
            <div className="playBtn__btn"></div>

            </div>
        </div>
    );
};

export default PlayButton;
