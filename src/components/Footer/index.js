import React from 'react';
import "./index.scss"
const Footer = () => {
    return (
        <div className="footer">
            <div className="footer__wrapper">
                <div className="footer__contacts">
                    <p>Украина, г.Киев, Шахтерская 9</p>
                    <p>+380&nbsp;63&nbsp;660&nbsp;47&nbsp;93</p>
                    <p>barista.kievx@gmail.com</p>
                </div>
                <div className="footer__title">Контакты</div>
                <div className="footer__schedule">
                    <p>Время работы:</p>
                    <p>Пн-Пт. 9:00 - 20:00</p>
                    <p>Cб. 11:00 - 15:00</p>
                    <p>Вс. Выходной</p>
                </div>
            </div>
        </div>
    );
};

export default Footer;
