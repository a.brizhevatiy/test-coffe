import React, {useState} from 'react';
import Logo from "../../Images/main-logo-gold.png"
import {useDispatch, useSelector} from "react-redux";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faSearch, faArrowCircleRight} from '@fortawesome/free-solid-svg-icons'
import {showCart} from "../../store/Cart/actions";
import "./index.scss"

const Navigation = () => {
    const [hide, setHide] = useState("navigation__hide");
    const [showSearch, setShowSearch] = useState(false)
    const [searchContent, setSearchContent] = useState("")
    const dispatch = useDispatch()
    const searchIcon = <FontAwesomeIcon icon={faSearch}/>
    const arrowIcon = <FontAwesomeIcon icon={faArrowCircleRight}/>
    const searchAction = () => {
        setShowSearch(false)
        alert(`Searching ${searchContent}`)
        setSearchContent("")
    }
    const hideHandler = () => {
        setHide(hide === "navigation__hide" ? "" : "navigation__hide");
    }

    const searchChangeHandler = (e) => {
        setSearchContent(e.target.value)
    }

    const cart = useSelector(state => state.cart.cart)
    return (
        <div className="navigation__wrapper" id={"main"}>
            <div className="navigation">
                <div className="navigation__logo">
                    <img src={Logo} alt="logo"/>
                </div>
                <div className="navigation__menu">
                    <div className="navigation__icon navigation-icon">
                        <div className="navigation-icon__lines" onClick={hideHandler}>
                            <div className="navigation-icon__line"></div>
                            <div className="navigation-icon__line navigation-icon__line_middle-margin"></div>
                            <div className="navigation-icon__line"></div>
                        </div>
                    </div>
                    <ul className={`navigation__items ${hide}`}>
                        <li className="navigation__item" onClick={() => setHide("navigation__hide")}><a
                            href="#main">Главная</a></li>
                        <li className="navigation__item" onClick={() => setHide("navigation__hide")}><a href="#about">О
                            Нас</a></li>
                        <li className="navigation__item" onClick={() => setHide("navigation__hide")}><a
                            href="#shop">Магазин</a></li>
                        <li className="navigation__item" onClick={() => setHide("navigation__hide")}><a
                            href="#help">Помощник</a></li>
                        <li className="navigation__item" onClick={() => setHide("navigation__hide")}><a href="#courier">Доставка
                            и оплата</a></li>
                        <li className="navigation__item" onClick={() => setHide("navigation__hide")}><a
                            href="#contacts">Контакты</a></li>
                        <li className="navigation__item" onClick={(e) => {
                            // setHide("navigation__hide")
                            if (!e.target.classList.contains("navigation__item--input")) {
                                setShowSearch(!showSearch)
                            }
                        }}>{searchIcon}
                            {showSearch &&
                            <div className={"navigation__search"}><input className={"navigation__item--input"}
                                                                         type="text" placeholder={"Поиск"}
                                                                         value={searchContent}
                                                                         onChange={searchChangeHandler}/>
                                <div className={"navigation__search--action"} onClick={searchAction}>{arrowIcon}</div>
                            </div>}
                        </li>
                    </ul>
                </div>
                <div className="navigation__services">
                    <div className="navigation__cart" onClick={() => dispatch(showCart(true))}>Корзина
                        <div className="navigation__cart--val">{cart.length}</div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Navigation;