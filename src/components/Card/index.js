import React, {useState} from 'react';
import ModalCard from "../ModalCard";
import Button from "../Button";
import Labels from "../Labels";
import "./index.scss"


const Card = ({item}) => {

    const [showCard, setShowCard] = useState(false);
    return (
        <div className="card__wrapper">
            {showCard && <ModalCard item={item} openClose={setShowCard}/>}
            <div className="card">
                <Labels sale={item?.actions?.sale}
                        newOne={item?.actions?.newOne}
                        hit={item?.actions?.hit}
                        recomend={item?.actions?.recomend}/>
                <div className="card__image">
                    <img src={item?.picture} alt="item"/>
                </div>
                <div className="card__title">{item?.title}</div>
                <div className="card__ratio"></div>
                <div className="card__oldPrice">{item?.oldPrice}{item?.oldPrice && "₴"}</div>
                <div className="card__price"><span className="card__price--digits">{item?.price}</span>₴</div>
                <div className="card__button">
                    <Button text={item?.inStock === 0 ? "Ожидаем" : "Купить"} onClick={() => setShowCard(true)} disabled={item?.inStock === 0}/>
                </div>
            </div>
        </div>
    );
};

export default Card;
