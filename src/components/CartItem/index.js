import React from 'react';
import InputNumber from "../Common/InputNumber";
import {saveCart} from "../../store/Cart/actions"
import {useDispatch, useSelector} from "react-redux";
import Cross from "../Cross";

const Cart = ({data}) => {
    const dispatch = useDispatch()
    const cart = useSelector(store => store.cart.cart)

    let price = data.item.price;
    if (data.amount > 99) {
        price = data.item.price100;
    } else if (data.amount > 29) {
        price = data.item.price30;
    } else if (data.amount > 9) {
        price = data.item.price10;
    } else if (data.amount > 4) {
        price = data.item.price5;
    } else if (data.amount > 2) {
        price = data.item.price3;
    } else {
        price = data.item.price
    }

    const onchangeHandler = (number) => {
        if (isNaN(number)) {
            number = 0;
        }

        const item = cart.findIndex(el => {
            return el.item.id === data.item.id
        })

        cart[item].amount = parseInt(number)
        dispatch(saveCart([...cart]))
    }
    const onRemoveHandler = () => {
        const newArr = cart.filter(el => {
            return el.item.id !== data.item.id
        })
        dispatch(saveCart([...newArr]))
    }

    return (
        <div className="cart__content cart__bordered">
            <div className="cart__data">
                <Cross className="cart__data--cross" close={onRemoveHandler}/>
            </div>
            <div className="cart__data">
                <img src={data?.item?.picture} alt=""/>
            </div>
            <div className="cart__data">{data?.item?.title}</div>
            <div className="cart__data">{data?.item?.weight}</div>
            <div className="cart__data"><InputNumber onChange={onchangeHandler} name={"quantity"}
                                                     initialNumber={data?.amount}/>
            </div>
            <div className="cart__data">{price}₴</div>
            <div className="cart__data">{data?.amount * price}₴</div>
        </div>
    );
};

export default Cart;
