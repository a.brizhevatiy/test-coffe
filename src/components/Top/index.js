import React from 'react';
import "./index.scss"
import Svg from "../Svg/Svg";
import CoffeePlant from "../Svg/Images/CoffeePlant"

const Top = () => {
    return (
        <div className="top__wrapper">
            <div className='top'>
                <Svg
                    className={'PositionMobileSimplePageSvg'}
                    viewBox={'-0 -0 113.76 210.48'}
                    path={<CoffeePlant/>}
                />
                <div className="top__text">
                    <div className='top__text--title'>barista .kiev</div>
                    <div className='top__text--title'>coffee</div>
                    <div className="top__text--last">
                        <div className='top__text--title'>center</div>
                        <div className="top__text--position">Кофе №1 в Украине</div>
                    </div>
                    <div className="top__comment">
                        <div className="top__comment--text">крафтовый</div>
                        <div className="top__comment--text">производитель</div>
                        <div className="top__comment--text">кофе</div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Top;
