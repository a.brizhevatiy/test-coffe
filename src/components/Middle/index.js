import React from 'react';
import Logo from "../../Images/main-logo-gold.png"
import owner from "../../Images/prihodko2.png"
import "./index.scss"

const Middle = () => {
    return (
        <div className="middle" id={"about"}>
            <div className="middle__wrapper">
                <div className="middle__text">
                    <div className="middle__title">Напиток кофейного дерева.</div>
                    <div className="middle__description">Вкус, удовольствие от которого я не мог забыть весь день…</div>
                    <div className="middle__review">До – я не особо разбирался во вкусах, пил американо, капучино... как
                        все… Но, однажды мне посчастливилось попробовать такой вкусный кофе, впечатление от которого
                        создало настроение на весь день. И я не мог его забыть… Так родилась идея узнать о кофе как
                        можно больше: что влияет на вкус, какие зерна лучшие, как их обжаривать, упаковывать и готовить.
                        Я обучился искусству бариста сам, создал школу для обучения баристов, изучил технологию
                        производства. Теперь я знаю о кофе все и делом моей жизни стало делиться удовольствием от вкуса
                        настоящего премиального кофе.
                    </div>

                </div>
                <div className="middle__logo">
                    <img src={Logo} alt="logo"/>
                </div>
                <div className="middle__pic">
                    <img src={owner} alt="" className="middle__img"/>
                </div>
            </div>
        </div>
    );
};

export default Middle;
