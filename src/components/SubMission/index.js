import React from 'react';
import Button from "../Button"
import "./index.scss"

const SubMission = ({title, text, btnText, btnFunc, pictures, reverse}) => {

    return (
        <div className="submission">
            <div className="submission__wrapper">
                <div className={`submission__info ${reverse ? 'submission__second' : ""}`}>
                    {/*<div className="submission__title--info">Recommendations</div>*/}
                    <h4 className="submission__title">{title}</h4>
                    <p className="submission__text">{text}</p>
                    <div className="submission__btn col-sm-12">
                        <Button text={btnText} onClick={btnFunc}/>
                    </div>
                </div>
                <div className={`submission__images  ${reverse ? 'submission__first' : ""}`}>
                    <div className={`submission__image ${reverse ? 'submission__image--1reverse' : "submission__image--1"}`} style={{
                        backgroundImage: `url(${pictures[0]})`,
                        backgroundRepeat: "repeat",
                        backgroundSize: "70px"
                    }}>
                    </div>
                    <div className={`submission__image ${reverse ? 'submission__image--2reverse' : "submission__image--2"}`}>
                        <img src={pictures[1]} alt=""/>
                    </div>
                    <div className={`submission__image ${reverse ? 'submission__image--3reverse' : "submission__image--3"}`}>
                        <img src={pictures[2]} alt=""/>
                    </div>

                </div>
            </div>
        </div>
    );
};

export default SubMission;
