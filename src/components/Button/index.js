import React from 'react';
import "./index.scss"

const Button = ({text = "купить", onClick, className = "", disabled}) => {
    const disabledBtn = disabled ? "disBtn" : ""
    return (
        <div className={`button ${disabledBtn}`}>
            <button className={`button__content border-button ${className}`} onClick={onClick}>{text}</button>
        </div>
    );
};

export default Button;
