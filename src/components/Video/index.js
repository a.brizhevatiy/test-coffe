// taken from https://github.com/videojs/video.js/blob/master/docs/guides/react.md
import React from 'react';
import videojs from 'video.js';
import previewImg from "../../Images/coffee-big.webp"
import PlayButton from "../PlayButton";
import "video.js/dist/video-js.css"
import "./index.scss"

export default class VideoPlayer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {player: false};
    }

    // componentDidMount() {
    //     // instantiate video.js
    //     this.player = videojs(this.videoNode, this.props, function onPlayerReady() {
    //         // console.log('onPlayerReady', this);
    //     });
    // }
    //
    // // destroy player on unmount
    // componentWillUnmount() {
    //     if (this.player) {
    //         this.player.dispose();
    //     }
    // }

    // wrap the player in a div with a `data-vjs-player` attribute
    // so videojs won't create additional wrapper in the DOM
    // see https://github.com/videojs/video.js/pull/3856
    render() {
        return (
            <div className="videoplayer">
 {/*<div data-vjs-player className="videoplayer__wrapper">*/}
 {/*                   <video ref={node => (this.videoNode = node)} className="video-js videoplayer__video"/>*/}
 {/*               </div>*/}
                {!this.state.player && <div className="videoplayer__preview">
                    <img src={previewImg} alt=""/>
                </div>}
                {!this.state.player && <PlayButton func={() => console.log("Let's play!")}/>
                }</div>
        );
    }
}
