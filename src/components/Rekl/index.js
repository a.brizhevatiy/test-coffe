import React from 'react';
import RekItem from "../RekItem"

import "./index.scss"

const Rekl = () => {
    const rekContent = [{
        title: "Оплата",
        text: ["Наличными при самовывозе", "Безналичный расчет (р/с для ООО, ФОП)", "Наложенный платеж при получении товара", "Оплата картой"]
    }, {
        title: "Доставка",
        text: ["Собственная доставка по Киеву", "Доставка Новой почтой", "Доставка Курьерской службой"]
    }, {
        title: "Гарантия",
        text: ["Мы уверены на 100% в каждом товаре магазина", "Если Вам не подошел товар - его всегда можно вернуть"]
    }]


    return (
        <div className="rekl__wrapper" id={"courier"}>
        <div className="rekl">
            {rekContent.map((elem, i) => {
                return (
                    <RekItem title={elem.title} text={elem.text} key={i}/>
                )
            })}
        </div>
        </div>
    );
};

export default Rekl;
