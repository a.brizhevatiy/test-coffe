import React from 'react';
import "./index.scss"

const Cross = ({close, className}) => {
    return (
        <div className={`cross ${className}`} onClick={close}>
        </div>
    );
};

export default Cross;
