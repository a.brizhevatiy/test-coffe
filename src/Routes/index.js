import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import Create from "../components/Create";
import Page from "../Pages/Page";

export default function Router() {
    return (
        <>
            <Switch>
                <Route exact path="/" component={Page} />
                <Route exact path="/admin" component={Create} />
                <Redirect exact from='*' to='/' />
            </Switch>
        </>
    );
}
